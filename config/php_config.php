<?php

# ambientes de desenvolvimento (caso nao esteja aqui, é produção)
$devHostnames = [
    '127.0.0.1',
    'localhost'
];

date_default_timezone_set('America/Sao_Paulo');

error_reporting(E_ALL | E_STRICT);

# pega o hostname da máquina em que a aplicação está rodando
$hostname = $_SERVER['REMOTE_ADDR'];

if(in_array($hostname, $devHostnames)){
    define('ENV', 'dev');
    require_once APP_ROOT_PATH.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'env'.DIRECTORY_SEPARATOR.'dev.php';
} else {
    define('ENV', 'prod');
    require_once APP_ROOT_PATH.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'env'.DIRECTORY_SEPARATOR.'prod.php';
}
