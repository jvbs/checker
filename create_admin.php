<?php
require_once 'init.php';

$name = 'Administrador Geral DF Checker';
$nickname = 'DF Checker Admin';
$email = 'admin@dfchecker.com.br';
$user_id = '31819255867';
$password = 'admin@jv';
$passwordHash = Hash::password($password);
$status = 1;
$user_level = 1;
$date = date('Y-m-d H:i:s');

$sql = "INSERT INTO users(name, nickname, email, user_id, password, status, user_level, created_by, created_at, updated_by, updated_at)
        VALUES (:name, :nickname, :email, :user_id, :password, :status, :user_level, :created_by, :created_at, :updated_by, :updated_at)";


$DB = new DB;

$stmt = $DB->prepare($sql);

$stmt->bindParam(':name', $name);
$stmt->bindParam(':nickname', $nickname);
$stmt->bindParam(':email', $email);
$stmt->bindParam(':user_id', $user_id);
$stmt->bindParam(':password', $passwordHash);
$stmt->bindParam(':status', $status, PDO::PARAM_INT);
$stmt->bindParam(':user_level', $user_level, PDO::PARAM_INT);
$stmt->bindParam(':created_by', $user_id);
$stmt->bindParam(':updated_by', $user_id);
$stmt->bindParam(':created_at', $date);
$stmt->bindParam(':updated_at', $date);

if($stmt->execute()){
    echo "Usuário admin criado com sucesso";
} else {
    echo "Erro ao criar usuário admin";
    echo "<br><br>";
    $error = $stmt->errorInfo();
    echo $error[2];
}
