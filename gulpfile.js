const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const connect = require('gulp-connect');
var uglify = require('gulp-uglify');

// SASS Task
gulp.task('sass', function() {
    return gulp.src('sass/checker.sass')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/css'))
});

// SASS Pages Task
gulp.task('sass-pages', function() {
    return gulp.src('sass/pages/*.sass')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(postcss([ autoprefixer() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/css/pages/**'))
});

// Minify CSS Task
gulp.task('minify-css', function() {
    return gulp.src(['assets/css/**/*.css', '!assets/css/**/*.min.css'])
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./assets/css'))
    .pipe(connect.reload());
});

// All CSS Files Task
gulp.task('css-files', gulp.series('sass','sass-pages','minify-css'));

// Minify JS Task
gulp.task('minify-js', function(){
    return gulp.src(['assets/js/**/*.js', '!assets/js/**/*.min.js'])
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./assets/js'))
});

// Create Server Task
gulp.task('server', function(){
    return connect.server({
    root: 'checker',
    livereload: true
    });
});

// Live reload
gulp.task('reload', function(){
    return gulp.src('./checker/**/*.*')
    .pipe(connect.reload());
});

// Watch Task
gulp.task('watch', function(){
    gulp.watch(['sass/**/*.sass'], gulp.series('css-files'));
    // gulp.watch(['assets/js/**/*.js'], gulp.series('minify-js'));
});

// Default Gulp Exec
gulp.task('default', gulp.series('watch'));
