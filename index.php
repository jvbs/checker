<?php
require_once 'init.php';

$configuration = [
    'settings' => [
        'determineRouteBeforeAppMiddleware' => true,
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false
    ],
];

$c = new \Slim\Container($configuration);

// error 404
$c['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $notFoundPage = file_get_contents('lib/Views/404.php');
        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/html')
            ->write($notFoundPage);
    };
};

$app = new \Slim\App($c);

// homepage
$app->get('/', function(){
    \Controllers\PagesController::home();
});

// configuracoes > meus dados
$app->get('/configuracoes/meus-dados', function(){
    \View::make('meus-dados');
});

// novo cliente
$app->get('/cadastrar-clientes', function(){
    \Controllers\ClientController::create();
});

// processa formulario cliente
$app->post('/cadastrar-clientes', function(){
    \Controllers\ClientController::store();
});

// novo usuario
$app->get('/novo-usuario', function(){
    \Controllers\UserController::create();
});

// processa formulario novo usuario
$app->post('/novo-usuario', function(){
    \Controllers\UserController::store();
});

// cadastro finalizado
$app->get('/cadastro-finalizado', function(){
    \View::make('cadastro-finalizado');
});

$app->get('/nova-ordem-de-servico', function(){
    \Controllers\ServiceOrderController::create();
});

$app->get('/executar-ordem-de-servico', function(){
    \Controllers\ServiceOrderController::execute();
});

// login
$app->map(['GET', 'POST'], '/login', function(){
    \Controllers\SessionsController::login();
});

// logout
$app->get('/logout', function(){
    \Controllers\SessionsController::logout();
});

$app->run();
