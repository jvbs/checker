<?php
namespace Controllers;

class ClientController {

    public static function create(){
        \View::make('cadastrar-clientes');
    }

    public static function store(){
            // valores recebidos do formulario
            // 1. dados tabela user
            $name = isset($_POST['name']) && !empty($_POST['name']) ? addslashes($_POST['name']) : NULL;
            $nickname = isset($_POST['first-name']) && !empty($_POST['first-name']) ? addslashes($_POST['first-name']) : NULL;
            $user_id = isset($_POST['user-id']) && !empty($_POST['user-id']) ? preg_replace("/\D+/", "", $_POST['user-id']) : NULL;
            $company_id = isset($_POST['company-id']) && !empty($_POST['company-id']) ? preg_replace("/\D+/", "", $_POST['company-id']) : NULL;
            $birthdate = isset($_POST['birthdate']) && !empty($_POST['birthdate']) ? \Models\DatabaseModel::convertDateToDB($_POST['birthdate']) : NULL;
            $email = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : NULL;
            $password = isset($_POST['password']) && !empty($_POST['password']) ? $_POST['password'] : NULL;
            $passwordConfirmation = isset($_POST['passwordConfirmation']) && !empty($_POST['passwordConfirmation']) ? $_POST['passwordConfirmation'] : NULL;
            // 2. dados tabela client
            $company_position = isset($_POST['company-position']) && !empty($_POST['company-position']) ? $_POST['company-position'] : NULL;
            $telephone = isset($_POST['telephone']) && !empty($_POST['telephone']) ? $_POST['telephone'] : NULL;
            $celphone = isset($_POST['celphone']) && !empty($_POST['celphone']) ? $_POST['celphone'] : NULL;
            $fantasy_name = isset($_POST['fantasy-name']) && !empty($_POST['fantasy-name']) ? $_POST['fantasy-name'] : NULL;
            $company_name = isset($_POST['company-name']) && !empty($_POST['company-name']) ? $_POST['company-name'] : NULL;
            $address = isset($_POST['address']) && !empty($_POST['address']) ? $_POST['address'] : NULL;
            $address_number = isset($_POST['number']) && !empty($_POST['number']) ? $_POST['number'] : NULL;
            $complement = isset($_POST['complement']) && !empty($_POST['complement']) ? $_POST['complement'] : '';
            $general_information = isset($_POST['general-information']) && !empty($_POST['general-information']) ? $_POST['general-information'] : '';

            // array de erros para exibição
            $hasErrors = false;
            $errors = [];

            if($name == NULL){
                $errors[] = "Nome do cliente não preenchido";
                $hasErrors = true;
            }

            if($nickname == NULL){
                $errors[] = "Primeiro nome do cliente não preenchido";
                $hasErrors = true;
            }

            if($user_id == NULL){
                $errors[] = "CPF não preenchido";
                $hasErrors = true;
            }

            if($company_id == NULL){
                $errors[] = "CNPJ não preenchido";
                $hasErrors = true;
            }

            if($birthdate == NULL){
                $errors[] = "Data de Nascimento não preenchida";
                $hasErrors = true;
            }

            if($email == NULL){
                $errors[] = "E-mail não preenchido";
                $hasErrors = true;
            }

            if($password == NULL){
                $errors[] = "Senha não preenchida";
                $hasErrors = true;
            }

            if($password != $passwordConfirmation){
                $errors[] = "Senhas não coincidem";
                $hasErrors = true;
            }

            if($company_position == NULL){
                $errors[] = "Cargo não preenchido";
                $hasErrors = true;
            }

            if($telephone == NULL){
                $errors[] = "Telefone para Contato não preenchido";
                $hasErrors = true;
            }

            if($celphone == NULL){
                $errors[] = "Celular não preenchido";
                $hasErrors = true;
            }

            if($fantasy_name == NULL){
                $errors[] = "Nome Fantasia não preenchido";
                $hasErrors = true;
            }

            if($company_name == NULL){
                $errors[] = "Razão Social não preenchida";
                $hasErrors = true;
            }

            if($address == NULL){
                $errors[] = "Endereço não preenchido";
                $hasErrors = true;
            }

            if($address_number == NULL){
                $errors[] = "Número do endereço não preenchido";
                $hasErrors = true;
            }

            if($hasErrors){
                return \View::make('cadastrar-clientes', compact('errors'));
            }

            $sql = "INSERT INTO clients(company_id, user_id, company_position, telephone, celphone, fantasy_name, company_name, address, address_number, complement, general_information, created_by, created_at, updated_by, updated_at) VALUES (:company_id, :user_id, :company_position, :telephone, :celphone, :fantasy_name, :company_name, :address, :address_number, :complement, :general_information, :created_by, :created_at, :updated_by, :updated_at);";
//INSERT INTO users(name, nickname, email, company_id, user_id, birthdate, password, status, user_level, created_by, created_at, updated_by, updated_at) VALUES (:name, :nickname, :email, :company_id, :user_id, :birthdate, :password, :status, :user_level, :created_by, :created_at, :updated_by, :updated_at);
            $DB = new \DB;
            $stmt = $DB->prepare($sql);

            $hashedPassword = \Hash::password($password);
            $user = \Auth::isLogged();
            $id = $user->getUserId();
            $date = date('Y-m-d H:i:s');
            $user_level = \Models\Client::CLIENT_LEVEL;

            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':nickname', $nickname);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':birthdate', $birthdate);
            $stmt->bindParam(':password', $hashedPassword);
            $stmt->bindValue(':status', \Models\User::STATUS_ACTIVE, \PDO::PARAM_INT);
            $stmt->bindParam(':user_level', $user_level);
            $stmt->bindParam(':created_by', $id);
            $stmt->bindParam(':created_at', $date);
            $stmt->bindParam(':updated_by', $id);
            $stmt->bindParam(':updated_at', $date);
            $stmt->bindParam(':company_position', $company_position);
            $stmt->bindParam(':telephone', $telephone);
            $stmt->bindParam(':celphone', $celphone);
            $stmt->bindParam(':fantasy_name', $fantasy_name);
            $stmt->bindParam(':company_name', $company_name);
            $stmt->bindParam(':address', $address);
            $stmt->bindParam(':address_number', $address_number);
            $stmt->bindParam(':complement', $complement);
            $stmt->bindParam(':general_information', $general_information);

            $params = array($user_id, $company_position, $telephone, $celphone, $fantasy_name, $company_name, $address, $address_number, $complement, $general_information, $id, $date, $id, $date);

            // $stmt->debugDumpParams();

            echo strtr($sql, $params);

            // if($stmt->execute()){
            //     // em vez de apenas exibir uma mensagem de sucesso, faremos um redirecionamento.
            //     // isso é melhor pois evita que o usuário atualize a página e crie uma nova conta
            //     //redirect(getBaseURL().'/cadastro-finalizado');
            // }
            // } else {
            //     list($error, $sgbdErrorCode, $sgbdErrorMessage) = $stmt->errorInfo();

            //     if($sgbdErrorCode == 1062){
            //         // erro 1062 é o código do MySQL de violação de chave única
            //         // veja mais em: http://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html

            //         if(preg_match( "/for key .?user_id/iu", $sgbdErrorMessage)){
            //             // cpf já em uso
            //             $errors[] = "CPF já está em uso";
            //         } elseif(preg_match( "/for key .?company_id/iu", $sgbdErrorMessage)) {
            //             // cnpj já em uso
            //             $errors[] = "CNPJ já está em uso";
            //         } else {
            //             // e-mail já em uso
            //             $errors[] = "E-mail já está em uso";
            //         }
            //     }
            //     return \View::make('novo-usuario', compact('errors'));
            // }


    }
}
