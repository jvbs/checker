<?php
namespace Controllers;

class ServiceOrderController {
    public static function create(){
        \View::make('nova-ordem-servico');
    }

    public static function execute(){
        \View::make('executar-ordem-servico');
    }

    public static function serviceOrderNumber(){
        return substr(date('YmdHi').rand(),0,15);
    }
}
