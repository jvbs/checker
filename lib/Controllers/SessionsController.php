<?php
namespace Controllers;

class SessionsController {
    public static function login(){
        if ($_SERVER['REQUEST_METHOD'] == 'POST'){
            self::processLoginForm();
        } else {
            self::showLoginForm();
        }
    }

    // função que mostra o formulário de login
    protected static function showLoginForm(){
        \View::makeLogin();
    }

    protected static function processLoginForm(){

        $cnpj = isset($_POST['cnpj']) && !empty($_POST['cnpj']) ? preg_replace("/\D+/", "", $_POST['cnpj']) : null;
        $cpf = isset($_POST['cpf']) && !empty($_POST['cpf']) ? preg_replace("/\D+/", "", $_POST['cpf']) : null;
        $password = isset($_POST['password']) && !empty($_POST['password']) ? $_POST['password'] : null;
        $hashedPassword = \Hash::password($password);

        $errors = [];
        if(empty($cnpj) || empty($cpf) || empty($password)){
            $errors[] = 'Informe seu <strong>CPF/CNPJ</strong> ou sua <strong>senha</strong>.';
        }


        if(count($errors) > 0){
            return \View::makeLogin(compact('errors'));
        }

        $DB = new \DB;
        $sql = "SELECT id, password, status FROM users WHERE user_id = :user_id and company_id = :company_id";
        $stmt = $DB->prepare($sql);
        $stmt->bindParam(':user_id', $cpf);
        $stmt->bindParam(':company_id', $cnpj);

        $stmt->execute();

        $rows = $stmt->fetchAll(\PDO::FETCH_OBJ);

        if(count($rows) <= 0 || $rows[0]->password != $hashedPassword){
            $errors[] = '<strong>Usuário</strong> ou <strong>senha</strong> inválidos!';
        } else {
            $user = $rows[0];

            // busca os dados do usuário para criar os dados no cookie
            $objUser = new \Models\User;
            $objUser->find($user->id);

            // gera um token de acesso
            $token = $objUser->generateToken();

            // salva o cookie com os dados do usuário
            self::saveSessionCookieForUser($objUser);

            // redireciona para a página inicial
            redirect(getBaseURL());
        }

        if(count($errors) > 0){
            return \View::makeLogin(compact('errors'));
        }
    }

    public static function saveSessionCookieForUser(\Models\User $user){
        $cookieData = [
            'id' => $user->getId(),
            'token' => $user->getToken(),
        ];
        // 2592000 = 60 * 60 * 24 * 30, ou seja, 30 dias em segundos
        setcookie(AUTH_USER_COOKIE_NAME, serialize($cookieData), time() + 2592000);
    }

    public static function extractCookieInfo(){
        if(!isset($_COOKIE[AUTH_USER_COOKIE_NAME])){
            return null;
        }
        $data = unserialize($_COOKIE[AUTH_USER_COOKIE_NAME]);
        return $data;
    }

    public static function logout(){
        // remove o cookie de autenticação
        self::destroySessionCookie();

        // redireciona para a página inicial
        redirect(getBaseURL().'/login');
    }

    public static function destroySessionCookie(){
        setcookie(AUTH_USER_COOKIE_NAME, '', 1);
    }
}
