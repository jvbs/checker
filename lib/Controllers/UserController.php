<?php
namespace Controllers;

class UserController {

    public static function create(){
        \View::make('novo-usuario');
    }

    public static function store(){
            // valores recebidos do formulario
            $user_level = isset($_POST['user-type']) && !empty($_POST['user-type']) ? $_POST['user-type'] : NULL;
            $name = isset($_POST['name']) && !empty($_POST['name']) ? addslashes($_POST['name']) : NULL;
            $nickname = isset($_POST['first-name']) && !empty($_POST['first-name']) ? addslashes($_POST['first-name']) : NULL;
            $user_id = isset($_POST['user-id']) && !empty($_POST['user-id']) ? preg_replace("/\D+/", "", $_POST['user-id']) : NULL;
            $company_id = isset($_POST['company-id']) && !empty($_POST['company-id']) ? preg_replace("/\D+/", "", $_POST['company-id']) : NULL;
            $birthdate = isset($_POST['birthdate']) && !empty($_POST['birthdate']) ? \Models\DatabaseModel::convertDateToDB($_POST['birthdate']) : NULL;
            $email = isset($_POST['email']) && !empty($_POST['email']) ? $_POST['email'] : NULL;
            $password = isset($_POST['password']) && !empty($_POST['password']) ? $_POST['password'] : NULL;
            $passwordConfirmation = isset($_POST['passwordConfirmation']) && !empty($_POST['passwordConfirmation']) ? $_POST['passwordConfirmation'] : NULL;

            // array de erros para exibição
            $hasErrors = false;
            $errors = [];

            if($user_level == null){
                $errors[] = "Tipo de Usuário não selecionado";
                $hasErrors = true;
            }

            if($name == null){
                $errors[] = "Nome do usuário não preenchido";
                $hasErrors = true;
            }

            if($nickname == null){
                $errors[] = "Primeiro nome não preenchido";
                $hasErrors = true;
            }

            if($user_id == null){
                $errors[] = "CPF não preenchido";
                $hasErrors = true;
            }

            if($company_id == null){
                $errors[] = "CNPJ não preenchido";
                $hasErrors = true;
            }

            if($birthdate == null){
                $errors[] = "Data de Nascimento não preenchida";
                $hasErrors = true;
            }

            if(!\Models\DatabaseModel::isEmail($email) || $email == null){
                $errors[] = "E-mail não preenchido";
                $hasErrors = true;
            }

            if($password == null){
                $errors[] = "Senha não preenchida";
                $hasErrors = true;
            }

            if($password != $passwordConfirmation){
                $errors[] = "Senhas não coincidem";
                $hasErrors = true;
            }

            if($hasErrors){
                return \View::make('novo-usuario', compact('errors'));
            }

            $sql = "INSERT INTO users(name, nickname, email, company_id, user_id, birthdate, password, status, user_level, created_by, created_at, updated_by, updated_at) VALUES (:name, :nickname, :email, :company_id, :user_id, :birthdate, :password, :status, :user_level, :created_by, :created_at, :updated_by, :updated_at)";

            $DB = new \DB;
            $stmt = $DB->prepare($sql);

            $date = date('Y-m-d H:i:s');
            $hashedPassword = \Hash::password($password);
            $user = \Auth::isLogged();
            $id = $user->getUserId();

            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':nickname', $nickname);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':company_id', $company_id);
            $stmt->bindParam(':user_id', $user_id);
            $stmt->bindParam(':birthdate', $birthdate);
            $stmt->bindParam(':password', $hashedPassword);
            $stmt->bindValue(':status', \Models\User::STATUS_ACTIVE, \PDO::PARAM_INT);
            $stmt->bindParam(':user_level', $user_level);
            $stmt->bindParam(':created_by', $id);
            $stmt->bindParam(':created_at', $date);
            $stmt->bindParam(':updated_by', $id);
            $stmt->bindParam(':updated_at', $date);


            if($stmt->execute()){
                // em vez de apenas exibir uma mensagem de sucesso, faremos um redirecionamento.
                // isso é melhor pois evita que o usuário atualize a página e crie uma nova conta
                redirect(getBaseURL().'/cadastro-finalizado');
            } else {
                list($error, $sgbdErrorCode, $sgbdErrorMessage) = $stmt->errorInfo();

                if($sgbdErrorCode == 1062){
                    // erro 1062 é o código do MySQL de violação de chave única
                    // veja mais em: http://dev.mysql.com/doc/refman/5.5/en/error-messages-server.html

                    if(preg_match( "/for key .?user_id/iu", $sgbdErrorMessage)){
                        // cpf já em uso
                        $errors[] = "CPF já está em uso";
                    } elseif(preg_match( "/for key .?company_id/iu", $sgbdErrorMessage)) {
                        // cnpj já em uso
                        $errors[] = "CNPJ já está em uso";
                    } else {
                        // e-mail já em uso
                        $errors[] = "E-mail já está em uso";
                    }
                }
                return \View::make('novo-usuario', compact('errors'));
            }
    }
}

