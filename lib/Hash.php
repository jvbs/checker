<?php
class Hash {
    protected static $passwordHashAlg = 'sha512';

    public static function password($str){
        # concatenando a password com o salt
        $str .= PASSWORD_SALT;

        return hash(static::$passwordHashAlg, $str);
    }
}
