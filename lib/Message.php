<?php
class Message {
    public static function displayMessage($messageTitle, $messageBody, $messageType){
        return "<div class='alert alert-${messageType} alert-dismissible' style='margin:15px 10px -15px 10px'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <strong>{$messageTitle}</strong> {$messageBody}
                </div>";
    }
}
