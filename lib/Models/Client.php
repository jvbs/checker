<?php
namespace Models;

class Client extends BaseModel {
    protected $tableName = 'clients';

    const CLIENT_LEVEL = 3;

    protected $id;
    protected $company_id;
    protected $user_id;
    protected $company_position;
    protected $telephone;
    protected $celphone;
    protected $fantasy_name;
    protected $company_name;
    protected $address;
    protected $address_number;
    protected $complement;
    protected $general_information;

    public function getId(){
        return $this->id;
    }

    public function getUserId(){
        return $this->user_id;
    }

    public function _setUserId($user_id){
        $this->user_id = $user_id;
    }

    public function getCompanyId(){
        return $this->company_id;
    }

    public function _setCompanyId($company_id){
        $this->company_id = $company_id;
    }

    public function getCompanyPosition(){
        return $this->company_position;
    }

    public function _setCompanyPosition($company_position){
        $this->company_position = $company_position;
    }

    public function getTelephone(){
        return $this->telephone;
    }

    public function _setTelephone($telephone){
        $this->telephone = $telephone;
    }

    public function getCelphone(){
        return $this->celphone;
    }

    public function _setCelphone($celphone){
        $this->celphone = $celphone;
    }

    public function getFantasyName(){
        return $this->fantasy_name;
    }

    public function _setFantasyName($fantasy_name){
        $this->fantasy_name = $fantasy_name;
    }

    public function getCompanyName(){
        return $this->company_name;
    }

    public function _setCompanyName($company_name){
        $this->company_name = $company_name;
    }

    public function getAddress(){
        return $this->address;
    }

    public function _setAddress($address){
        $this->address = $address;
    }

    public function getAddressNumber(){
        return $this->address_number;
    }

    public function _setAddressNumber($address_number){
        $this->address_number = $address_number;
    }

    public function getComplement(){
        return $this->complement;
    }

    public function _setComplement($complement){
        $this->complement = $complement;
    }

    public function getGeneralInformation(){
        return $this->general_information;
    }

    public function _setGeneralInformation($general_information){
        $this->general_information = $general_information;
    }

    public function getCreatedBy(){
        return $this->created_by;
    }

    public function _setCreatedBy($created_by){
        $this->created_by = $created_by;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }

    public function _setCreatedAt($created_at){
        $this->created_at = $created_at;
    }

    public function getUpdatedBy(){
        return $this->updated_by;
    }

    public function _setUpdatedBy($updated_by){
        $this->updated_by = $updated_by;
    }

    public function getUpdatedAt(){
        return $this->created_at;
    }

    public function _setUpdatedAt($updated_at){
        $this->updated_at = $updated_at;
    }
}
