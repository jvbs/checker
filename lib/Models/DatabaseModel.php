<?php
namespace Models;

class DatabaseModel {
    public static function convertDateToDB($date){
        if(isset($date) && !empty($date)){
            $date = new \DateTime($date);
            $date = $date->format('Y-m-d');
        }
        return $date;
    }

    public static function convertDateFromDB($date){
        if(isset($date) && !empty($date)){
            $date = new \DateTime($date);
            $date = $date->format('d/m/Y');
        }
        return $date;
    }

    public static function isEmail($email){
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";
        return preg_match($pattern, $email);
    }

    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     * @return string The interpolated query
     */
    public static function interpolateQuery($query, $params){
        $keys = array();

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        #trigger_error('replaced '.$count.' keys');

        return $query;
    }
}
