<?php
namespace Models;

class User extends BaseModel {
    protected $tableName = 'users';

    const STATUS_ACTIVE = 1;

    protected $id;
    protected $name;
    protected $nickname;
    protected $email;
    protected $user_id;
    protected $user_profile_pic;
    protected $birthdate;
    protected $password;
    protected $token;
    protected $status;
    protected $user_level;
    protected $created_by;
    protected $created_at;
    protected $updated_by;
    protected $updated_at;

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function _setName($name){
        $this->name = $name;
    }


    public function getNickname(){
        return $this->nickname;
    }

    public function _setNickname($nickname){
        $this->nickname = $nickname;
    }


    public function getEmail(){
        return $this->email;
    }

    public function _setEmail($email){
        $this->email = $email;
    }


    public function getUserId(){
        return $this->user_id;
    }

    public function _setUserId($user_id){
        $this->userId = $user_id;
    }

    public function getUserProfilePic(){
        return $this->userProfilePic;
    }

    public function _setUserProfilePic($user_profile_pic){
        $this->userProfilePic = $user_profile_pic;
    }

    public function getBirthdate(){
        return DatabaseModel::convertDateFromDB($this->birthdate);
    }

    public function _setBirthdate($birthdate){
        $this->birthdate = $birthdate;
    }

    public function _setToken($token){
        $this->token = $token;
    }

    public function getToken(){
        return $this->token;
    }


    public function _setStatus($status){
        $this->status = $status;
    }

    public function getStatus(){
        return $this->status;
    }


    public function _setUserLevel($user_level){
        $this->user_level = $user_level;
    }

    public function getUserLevel(){
        return $this->user_level;
    }


    public function _setCreatedBy($created_by){
        $this->created_by = $created_by;
    }

    public function getCreatedBy(){
        return $this->created_by;
    }


    public function _setCreatedAt($created_at){
        $this->created_at = $created_at;
    }

    public function getCreatedAt(){
        return $this->created_at;
    }


    public function _setUpdatedBy($updated_by){
        $this->updated_by = $updated_by;
    }

    public function getUpdatedBy(){
        return $this->updated_by;
    }


    public function _setUpdatedAt($updated_at){
        $this->updated_at = $updated_at;
    }

    public function getUpdatedAt(){
        return $this->created_at;
    }

    public function generateToken(){
        $string = sprintf("%d%s%s", $this->id, $this->password, microtime(true));
        $token = md5($string);

        $this->updateToken($token);

        return $token;
    }

    public function updateToken($token){
        $this->token = $token;
        $now = date('Y-m-d H:i:s');

        $DB = new \DB;
        $sql = "UPDATE users SET token = :token, updated_at = :now WHERE id = :id";
        $stmt = $DB->prepare($sql);
        $stmt->bindParam(":token", $token);
        $stmt->bindParam(":now", $now);
        $stmt->bindParam(":id", $this->id, \PDO::PARAM_INT);

        $stmt->execute();
    }
}
