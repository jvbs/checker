<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono%7CArvo:400%7CRoboto:300,400,500,700%7CCookie" rel="stylesheet">
  <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/perfect-scrollbar.css">

  <link rel="stylesheet" href="../../assets/css/checker.min.css">
  <title>DF CHECKER</title>
</head>
<body>

<div class="full-page-container px-8">
  <div class=" text-center" style="margin-top:15vh;" >
    <div class="media-box d-inline-block media-box-lg bg-primary text-white mb-8" style="">
      <i>C</i>
    </div>

  </div>
  <div class="text-center">
    <h1 class="mb-4">Página não encontrada</h1>
    <h4 class="mb-4">Oops..</h4>
    <p class="mb-1">Parece que você tentou acessar um endereço que não existe.</p>
    <p class="mb-10">Para voltar à tela principal, clique no botão abaixo.</p>
    <p>
      <a class="btn btn-outline-primary" href="/">Voltar</a>
    </p>
  </div>
</div>
</body>
</html>
