<div class="container-fluid">
    <div class="page-presentation" style="margin-bottom: 25px">
        <h2>Cadastro de Clientes</h2>
        <p class="text-secondary">cadastro voltado exclusivamente para clientes pessoas físicas e jurídicas</p>
    </div>
<!--     <div class="row">
        <div class="col-lg-12 text-center">
            <div class="btn-group btn-group-toggle " style="margin: 5px 0px 25px 0px" data-toggle="buttons">
                <label class="btn btn-primary active">
                    <input type="radio" name="options" id="option1" autocomplete="off" checked><i class="fa fa-user"></i> Pessoa Física
                </label>
                <label class="btn btn-primary">
                    <input type="radio" name="options" id="option2" autocomplete="off"><i class="fa fa-users"></i> Pessoa Jurídica
                </label>
            </div>
        </div>
    </div> -->
    <form action="<?=getCurrentURL()?>" method="POST" id="form-pj">
        <div class="card">
            <div class="card-header">
                <h4>Informações Pessoa Física</h4>
            </div>
            <?php if(isset($errors) && count($errors) > 0): ?>
                <div class='alert alert-danger alert-dismissible' style='margin:0px 25px 0px 25px'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <ul>
                            <?php foreach($errors as $error): ?>
                                <li><?=$error;?></li>
                            <?php endforeach; ?>
                        </ul>
                </div>
            <?php endif; ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Nome</label>
                            <input type="text" class="form-control" value="" id="name" name="name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Primeiro nome</label>
                            <input type="text" class="form-control" value="" id="first-name" name="first-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CPF</label>
                            <input type="text" class="form-control" value="" id="user-id" name="user-id">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Data de Nascimento</label>
                            <input type="text" class="form-control" value="" id="birthdate" name="birthdate">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Cargo</label>
                            <input type="text" class="form-control" value="" id="company-position" name="company-position">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>E-mail</label>
                            <input type="text" class="form-control" value="" id="email" name="email">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Telefone de Contato</label>
                            <input type="text" class="form-control" value="" id="telephone" name="telephone">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Celular</label>
                            <input type="text" class="form-control" value="" id="celphone" name="celphone">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Senha</label>
                            <input type="password" class="form-control" value="" id="password" name="password" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Confirme sua senha</label>
                            <input type="password" class="form-control" value="" id="passwordConfirmation" name="passwordConfirmation" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-header">
                <h4>Informações Pessoa Jurídica</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Nome Fantasia</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Razão Social</label>
                            <input type="text" class="form-control" value="" id="company-name" name="company-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CNPJ</label>
                            <input type="text" class="form-control" value="" id="company-id" name="company-id">
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="form-group ">
                            <label>Endereço</label>
                            <input type="text" class="form-control" value="" id="address" name="address">
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6">
                        <div class="form-group ">
                            <label>Número</label>
                            <input type="text" class="form-control" value="" id="number" name="number">
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6">
                        <div class="form-group ">
                            <label>Complemento</label>
                            <input type="text" class="form-control" value="" id="complement" name="complement">
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12">
                        <div class="form-group ">
                            <label>Informações Gerais</label>
                            <textarea name="general-info" id="general-info" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success text-center" style="float:right" type="submit"><i class="fa fa-check-circle"></i> Cadastrar</button>
            </div>
        </div>
    </form>
</div>
