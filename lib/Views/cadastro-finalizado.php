<div class="full-page-container px-8">
    <div class=" text-center" style="margin-top:15vh;" >
        <div class="media-box d-inline-block media-box-lg bg-success text-white mb-8" style="">
          <i class="fa fa-check"></i>
        </div>
    </div>
    <div class="text-center">
        <h1 class="mb-4">Cadastro finalizado com sucesso!</h1>
        <h4 class="mb-4">Agora é simples</h4>
        <p class="mb-1">Solicite ao usuário cadastrado que teste suas credenciais de acesso.</p>
        <p class="mb-10">Para voltar à tela principal, clique no botão abaixo.</p>
        <p>
          <a class="btn btn-outline-primary" href="/">Voltar</a>
        </p>
    </div>
</div>
