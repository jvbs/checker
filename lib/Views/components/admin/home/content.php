<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-bar-chart" style="margin-right:5px"></i> Status dos Pedidos</h4>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-area-chart" style="margin-right:5px"></i> Análises Executadas (por data)</h4>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-users" style="margin-right:5px"></i> Status dos Clientes</h4>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-users" style="margin-right:5px"></i> Status dos Pedidos dos Analistas</h4>
            </div>
            <div class="card-body">

            </div>
        </div>
    </div>
</div>
