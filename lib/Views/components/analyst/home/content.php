<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-trophy"></i> Resultados</h4>
            </div>
            <div class="card-body">


            </div>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-tasks"></i> Ánalises para Hoje</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th style="text-align:center">#</th>
                            <th style="text-align:center">Prioridade</th>
                            <th style="text-align:center">Responsável</th>
                            <th style="text-align:center">Criado por</th>
                            <th style="text-align:center">Cliente</th>
                            <th style="text-align:center">Status</th>
                            <th style="text-align:center">Data Cadastro</th>
                            <th style="text-align:center">Ações</th>
                        </thead>
                        <tbody>
                            <tr style="text-align:center">
                                <td>201806150220147</td>
                                <td>
                                    <i class="fa fa-exclamation" style="color:#e53434"></i>
                                    <i class="fa fa-exclamation" style="color:#e53434"></i>
                                    <i class="fa fa-exclamation" style="color:#e53434"></i>
                                </td>
                                <td>João Vitor</td>
                                <td>DF Checker Admin</td>
                                <td>Multiplan</td>
                                <td>Pendente</td>
                                <td>14/06/2018</td>
                                <td><a href="#" style="color:#0c6831"><i class="fa fa-play"></i> Executar</a></td>
                            </tr>
                            <tr style="text-align:center">
                                <td>201806150220148</td>
                                <td>
                                    <i class="fa fa-exclamation" style="color:#e53434"></i>
                                    <i class="fa fa-exclamation" style="color:#e53434"></i>
                                </td>
                                <td>João Vitor</td>
                                <td>Elder Rotondo</td>
                                <td>Multiplan</td>
                                <td>Pendente</td>
                                <td>14/06/2018</td>
                                <td><a href="#" style="color:#0c6831"><i class="fa fa-play"></i> Executar</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
