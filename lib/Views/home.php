<div class="row">
    <div class="col-12">
        <h4 class="section-title" style="font-weight:300">
            Seja bem-vindo, <?=$user->getNickname();?>!
        </h4>
    </div>
</div>
<div class="container-fluid">
<?php
if($user->getUserLevel() == 1){
    require_once('components/admin/home/content.php');
} elseif($user->getUserLevel() == 2){
    require_once('components/analyst/home/content.php');
} else {
    require_once('components/client/home/content.php');
}
