<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="icon" type="image/x-icon" href="../assets/img/favicon.ico"> -->
    <!-- Plugins -->
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/perfect-scrollbar.css">
    <!-- Theme -->
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/css/checker.min.css">
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/css/pages/login.min.css">

    <title>DF Checker</title>
</head>
<body>
<div class="full-page-container">
    <div class="d-flex flex-row justify-content-center px-4" style="margin-top:9vh;" >
        <div class="card" style="width:28rem;">
            <div class="card-header py-3  d-flex justify-content-between align-items-center" style="border-bottom: 1px solid #eee;">
                <img src="<?=getBaseURL()?>/assets/img/df_checker_1.svg" style="width:100%" alt="Logo DF Checker">
            </div>
            <?php if(isset($errors) && count($errors) > 0):
                echo \Message::displayMessage('ERRO!', $errors[0], 'danger');
            endif; ?>
            <div class="card-body">
                <form class="" method="POST" action="<?=getCurrentURL()?>">
                    <div class="form-group mb-4 mt-2">
                        <label>CNPJ <span style="color:red">*</span></label>
                        <input type="text" autocomplete="off" class="form-control" id="cnpj" name="cnpj" placeholder="">
                    </div>
                    <div class="form-group mb-4 mt-2">
                        <label>CPF <span style="color:red">*</span></label>
                        <input type="text" autocomplete="off" class="form-control" id="cpf" name="cpf" placeholder="">
                    </div>
                    <div class="form-group mb-4">
                        <label>Senha <span style="color:red">*</span></label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="">
                    </div>
                    <!--<div class="form-check mb-2 py-2">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">Keep Me Logged In</span>
                        </label>
                    </div>-->
                    <div class="py-1">
                        <button type="submit" class="btn btn-info btn-block">Login</button>
                    </div>
                <!--<div class="py-0 mt-2 d-flex justify-content-around">
                    <button class="btn btn-flat-info"><i class="fa fa-twitter"></i></button>
                    <button class="btn btn-flat-danger"><i class="fa fa-google"></i></button>
                    <button class="btn btn-flat-info"><i class="fa fa-facebook"></i></button>
                    <button class="btn btn-flat-warning"><i class="fa fa-stack-overflow"></i></button>
                  </div> -->
                </form>
<!--                 <div class="py-2 px-0">
                    <p class="mb-0">
                        <button class=" btn btn-link pl-1">Esqueceu sua senha?</button>
                    </p>
                </div> -->
            </div>
        <!--<div class="card-footer">
            <button class=" btn btn-link pl-1">Don't have an account? Signup Now!</button>
        </div> -->
        </div>
    </div>
</div>
<script src="<?=getBaseURL()?>/assets/plugins/js/jquery.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/popper.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/bootstrap.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/perfect-scrollbar.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/jquery.mask.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/login.min.js"></script>
</body>
</html>
