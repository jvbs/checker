<div class="card">
    <div class="card-header">
        <h2>Meus Dados</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="profile-picture text-center">
                    <img src="http://via.placeholder.com/230x230" style="box-shadow:0px 0px 10px rgba(0, 0, 0, 0.10)" alt="">
                    <a href="#" style="background-color: red"><i class="fa fa-camera"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    <label>Nome</label>
                    <input type="text" class="form-control" value="<?=$user->getName();?>" id="name" name="name" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="form-group">
                    <label>Primeiro nome</label>
                    <input type="text" class="form-control" value="<?=$user->getNickname();?>" id="first-name" name="first-name" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="form-group">
                    <label>E-mail</label>
                    <input type="text" class="form-control" value="<?=$user->getEmail();?>" id="email" name="email" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="form-group">
                    <label>Data de Nascimento</label>
                    <input type="text" class="form-control" value="<?=$user->getBirthdate();?>" id="birthdate" name="birthdate" autocomplete="off">
                </div>
            </div>
        </div>
    </div>

    <div class="card-header">
        <h2>Alterar Senha</h2>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-2 col-sm-6">
                <div class="form-group">
                    <label>Nova senha</label>
                    <input type="password" class="form-control" value="" id="password" name="name" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="form-group">
                    <label>Repita a nova senha</label>
                    <input type="password" class="form-control" value="" id="password-repeat" name="first-name" autocomplete="off">
                </div>
            </div>
            <div class="col-lg-2">
                <button class="btn btn-success text-center" style="margin-top:30px" type="submit"><i class="fa fa-check-circle"></i> Alterar Dados</button>
            </div>
        </div>
    </div>
</div>
