<div class="container-fluid">
    <div class="page-presentation" style="margin-bottom: 25px">
        <h2>Nova Ordem de Serviço</h2>
        <p class="text-secondary">cadastro de abertura de uma nova ordem de serviço.</p>
    </div>

    <form action="<?=getCurrentURL()?>" method="POST" id="form-os">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-file-text-o"></i> Informações da OS</h4>
            </div>
            <?php if(isset($errors) && count($errors) > 0): ?>
                <div class='alert alert-danger alert-dismissible' style='margin:0px 25px 0px 25px'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <ul>
                        <?php foreach($errors as $error): ?>
                            <li><?=$error;?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Número da OS</label>
                            <input type="text" class="form-control" style="font-weight:bold;text-align:center" value="<?=\Controllers\ServiceOrderController::serviceOrderNumber();?>" id="name" name="name" disabled>
                        </div>
                    </div>

                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Status da OS</label>
                            <select name="status-os" class="form-control" id="">
                                <option value="">---Selecione---</option>
                                <option value="">Aprovada</option>
                                <option value="">Aguardando execução</option>
                                <option value="">Em atraso</option>
                                <option value="">Em execução</option>
                                <option value="">Finalizada</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Cliente</label>
                            <input type="text" class="form-control" value="" id="user-id" name="user-id">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Analista Responsável</label>
                            <select name="analista-executor" id="" class="form-control">
                                <option value="">---Selecione---</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Data do Cadastro</label>
                            <input type="text" class="form-control" value="<?=date('d/m/Y');?>" id="company-position" name="company-position">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Estimativa de Início</label>
                            <input type="text" class="form-control" value="" id="email" name="email">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Estimativa de Término</label>
                            <input type="text" class="form-control" value="" id="telephone" name="telephone">
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6">
                        <div class="form-group ">
                            <label>Nº Pesquisas</label>
                            <input type="number" min="1" max="100" class="form-control" value="" id="" name="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-header">
                <h4><i class="fa fa-users"></i> Informações do Pesquisado</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Nome Pesquisado</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CNPJ</label>
                            <input type="text" class="form-control" value="" id="company-name" name="company-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CPF</label>
                            <input type="text" class="form-control" value="" id="company-id" name="company-id">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <h4><i class="fa fa-search"></i> Itens de Pesquisa</h4>
            </div>
            <div class="card-body">
                <div class="row">




                    <div class="col-lg-6 col-sm-6">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="border:1px solid #e2e2e2;padding:5px;margin-top:10px">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h5 style="background-color:#f2f2f2;padding:5px 0px;"><input type="checkbox"> Modelo Alto</h5>
                                  <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><input type="checkbox"> Integridade/ Compliance</a>
                                  </h6>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                        <input type="checkbox"> O Setor de atuação do fornecedor é percebida por possuir um alto risco de corrupção?</br>
                                        <input type="checkbox"> A Empresa está envolvida em alguma investigação Federal?</br>
                                        <input type="checkbox"> Cadastro de empresas idôneas e suspensas (CEIS)</br>
                                        <input type="checkbox"> Consulta à lista de condenados por trabalho escravo</br>
                                        <input type="checkbox"> Análise da atividade econômica (CNAE)</br>
                                        <input type="checkbox"> Regularidade junto ao órgão fiscalizador competente (ANTT, CRM, OAB, CRC, CREA, etc.)</br>
                                        <input type="checkbox"> Documentações específicas de cada setor de fornecimento de bens e serviços</br>
                                        <input type="checkbox"> Cadastro de empresas punidas (CNEP)</br>
                                        <input type="checkbox"> Alvará de funcionamento</br>
                                        <input type="checkbox"> "Análise societária. Possui sócio/administrador da empresa ou parentes próximos que</br>
                                        <input type="checkbox"> trabalham ou que foram desligados nos últimos 3 anos da Multiplan?"</br>
                                        <input type="checkbox"> Consulta de realizações de doações politicas</br>
                                        <input type="checkbox"> Exposição em mídia adversa/negativa</br>
                                        <input type="checkbox"> Consulta ao COAF -Conselho de Controle de Atividades Financeiras</br>

                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><input type="checkbox"> Econômico/ Financeiro</a>
                                  </h6>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <input type="checkbox"> Protestos e títulos - Consulta em base aberta de dados públicos de Protestos</br>
                                        <input type="checkbox"> Faturamento anual</br>
                                        <input type="checkbox"> Litigante na Multiplan</br>
                                        <input type="checkbox"> Balanço patrimonial e DRE dos últimos dois anos</br>
                                        <input type="checkbox"> Fração de lucro líquido e saldo ativo</br>
                                        <input type="checkbox"> Risco de Crédito Completo</br>
                                        <input type="checkbox"> CADE - identificação de algum processo em andamento</br>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h6 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><input type="checkbox"> Jurídico/ Criminal/ Federal</a>
                                    </h6>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                      <div class="panel-body">
                                            <input type="checkbox"> Contrato social e/ou última alteração contratual consolidada e registrada na Junta Comercial; ou no caso de sociedade anônima, ato constitutivo ou estatuto</br>
                                            <input type="checkbox"> CND ISS (Serviço)</br>
                                            <input type="checkbox"> CND ICMS (Material)</br>
                                            <input type="checkbox"> CND INSS</br>
                                            <input type="checkbox"> CND FGTS</br>
                                            <input type="checkbox"> Processos judiciais criminais</br>
                                            <input type="checkbox"> Dívida ativa da união</br>
                                            <input type="checkbox"> Processos trabalhistas</br>
                                            <input type="checkbox"> Processos tributários</br>
                                            <input type="checkbox"> Processos civeis</br>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>







                    <div class="col-lg-6 col-sm-6">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="border:1px solid #e2e2e2;padding:5px;margin-top:10px">
                            <div class="panel panel-default">
                                <h5 style="background-color:#f2f2f2;padding:5px 0px;"><input type="checkbox" checked> Modelo Médio</h5>
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><input type="checkbox" checked> Integridade/ Compliance</a>
                                  </h6>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                  <div class="panel-body">
<input type="checkbox" checked> O Setor de atuação do fornecedor é percebida por possuir um alto risco de corrupção?</br>
<input type="checkbox" checked> A Empresa está envolvida em alguma investigação Federal?</br>
<input type="checkbox" checked> Cadastro de empresas idôneas e suspensas (CEIS)</br>
<input type="checkbox" checked> Consulta à lista de condenados por trabalho escravo</br>
<input type="checkbox" checked> Análise da atividade econômica (CNAE)</br>
<input type="checkbox" checked> Regularidade junto ao órgão fiscalizador competente (ANTT, CRM, OAB, CRC, CREA, etc.)</br>
<input type="checkbox" checked> Documentações específicas de cada setor de fornecimento de bens e serviços</br>
<input type="checkbox" checked> Cadastro de empresas punidas (CNEP)</br>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                  <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><input type="checkbox" checked> Econômico/ Financeiro</a>
                                  </h6>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
<input type="checkbox" checked> Protestos e títulos - Consulta em base aberta de dados públicos de Protestos</br>
<input type="checkbox" checked> Faturamento anual</br>
<input type="checkbox" checked> Litigante na Multiplan</br>
<input type="checkbox" checked> Balanço patrimonial e DRE dos últimos dois anos</br>
<input type="checkbox" checked> Fração de lucro líquido e saldo ativo</br>
<input type="checkbox" checked> Risco de Crédito (simples)</br>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSix">
                                    <h6 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><input type="checkbox" checked> Jurídico/ Criminal/ Federal</a>
                                    </h6>
                                </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                      <div class="panel-body">
<input type="checkbox" checked> Contrato social e/ou última alteração contratual consolidada e registrada na Junta Comercial; ou no caso de sociedade anônima, ato constitutivo ou estatuto</br>
<input type="checkbox" checked> CND ISS (Serviço)</br>
<input type="checkbox" checked> CND ICMS (Material)</br>
<input type="checkbox" checked> CND INSS</br>
<input type="checkbox" checked> CND FGTS</br>
<input type="checkbox" checked> Processos judiciais criminais</br>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>








                    <div class="col-lg-6 col-sm-6">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="border:1px solid #e2e2e2;padding:5px;margin-top:10px">
                            <div class="panel panel-default">
                                <h5 style="background-color:#f2f2f2;padding:5px 0px;"><input type="checkbox"> Modelo Baixo</h5>
                                <div class="panel-heading" role="tab" id="headingSeven">
                                  <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven"><input type="checkbox"> Integridade/ Compliance</a>
                                  </h6>
                                </div>
                                <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSeven">
                                  <div class="panel-body">
                                        <input type="checkbox">O Setor de atuação do fornecedor é percebida por possuir um alto risco de corrupção?</br>
                                        <input type="checkbox">A Empresa está envolvida em alguma investigação Federal?</br>
                                        <input type="checkbox">Cadastro de empresas idôneas e suspensas (CEIS)</br>
                                        <input type="checkbox">Consulta à lista de condenados por trabalho escravo</br>
                                        <input type="checkbox">Análise da atividade econômica (CNAE)</br>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingEight">
                                  <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"><input type="checkbox"> Econômico/ Financeiro</a>
                                  </h6>
                                </div>
                                <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                    <div class="panel-body">
                                        <input type="checkbox">Protestos e títulos - Consulta em base aberta de dados públicos de Protestos</br>
                                        <input type="checkbox">Faturamento anual</br>
                                        <input type="checkbox">Litigante na Multiplan</br>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingNine">
                                    <h6 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine"><input type="checkbox"> Jurídico/ Criminal/ Federal</a>
                                    </h6>
                                </div>
                                <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                      <div class="panel-body">
                                            <input type="checkbox">Contrato social e/ou última alteração contratual consolidada e registrada na Junta Comercial; ou no caso de sociedade anônima, ato constitutivo ou estatuto</br>
                                            <input type="checkbox">CND ISS (Serviço) / CND ICMS (Material)</br>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>






                    <div class="col-lg-6 col-sm-6">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="border:1px solid #e2e2e2;padding:5px;margin-top:10px">
                            <div class="panel panel-default">
                                <h5 style="background-color:#f2f2f2;padding:5px 0px;"><input type="checkbox"> Modelo PF</h5>
                                <div class="panel-heading" role="tab" id="headingEleven">
                                  <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven"><input type="checkbox"> Integridade/ Compliance</a>
                                  </h6>
                                </div>
                                <div id="collapseEleven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingEleven">
                                  <div class="panel-body">
                                        <input type="checkbox"> Informação do CPF e RG/Carteira de Motorista</br>
                                        <input type="checkbox"> Regularidade junto ao órgão fiscalizador competente (ANTT, CRM, OAB, CRC, CREA, etc.)</br>
                                        <input type="checkbox"> Aceite no Código de Ética </br>
                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTen">
                                  <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen"><input type="checkbox"> Econômico/ Financeiro</a>
                                  </h6>
                                </div>
                                <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                    <div class="panel-body">
                                        <input type="checkbox"> Comprovante de Dados Bancários, contendo as informações do Banco, Agência, Conta e CPF do titular</br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>






                    <div class="col-lg-6 col-sm-6">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="border:1px solid #e2e2e2;padding:5px;margin-top:10px">
                            <div class="panel panel-default">
                                <h5 style="background-color:#f2f2f2;padding:5px 0px;"><input type="checkbox"> Modelo Estrangeiro</h5>
                                <div class="panel-heading" role="tab" id="headingTwelve">
                                  <h6 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve" aria-expanded="true" aria-controls="collapseTwelve"><input type="checkbox"> Integridade/ Compliance</a>
                                  </h6>
                                </div>
                                <div id="collapseTwelve" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwelve">
                                  <div class="panel-body">
<input type="checkbox"> Aceite do Código de Conduta</br>
<input type="checkbox"> Check com a lista de países</br>
<input type="checkbox"> Exposição em mídia adversa/negativa</br>


                                  </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThirteen">
                                  <h6 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen"><input type="checkbox"> Econômico/ Financeiro</a>
                                  </h6>
                                </div>
                                <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirteen">
                                    <div class="panel-body">
<input type="checkbox"> Comprovante bancário em nome do fornecedor com informações de Banco, Agência, Conta (código swift ou ABA ou IBAN ou similar)</br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>





                </div>
            </div>
            <div class="card-header">
                <h4><i class="fa fa-usd"></i> Dados para Cotação</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Valor Unitário</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Valor Total</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-6">
                        <div class="form-group ">
                            <label>Desconto (%)</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Formas de Pagamento</label>
                            <select class="form-control" name="" id="">
                                <option value="">---Selecione---</option>
                                <option value=""></option>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Número da NF</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Data do Faturamento</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Nome do Solicitante</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>E-mail do Solicitante</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Telefone</label>
                            <input type="text" class="form-control" value="" id="fantasy-name" name="fantasy-name">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Comentários Gerais</label>
                            <textarea class="form-control" name="" id="" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                </div>

                <a  href="#" class="btn btn-success text-center" style="float:left"><i class="fa fa-paperclip"></i> Anexar</a>
                <button class="btn btn-success text-center" style="float:right" type="submit"><i class="fa fa-check-circle"></i> Criar OS</button>
                <a href="#" class="btn btn-info text-center" style="float:right;margin-right: 5px"><i class="fa fa-print"></i> Imprimir</a>
            </div>

            </div>
        </div>
    </form>
</div>
