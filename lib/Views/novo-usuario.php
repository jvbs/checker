<div class="container-fluid">
    <div class="page-presentation" style="margin-bottom: 25px">
        <h2>Cadastro de Usuários</h2>
        <p class="text-secondary">cadastro voltado exclusivamente para usuários DF Checker</p>
    </div>

    <form action="<?=getCurrentURL()?>" method="POST" id="form-pj">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-user-plus"></i> Formulário de Cadastro</h4>
            </div>
                <?php if(isset($errors) && count($errors) > 0): ?>
                    <div class='alert alert-danger alert-dismissible' style='margin:0px 25px 0px 25px'>
                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                            <ul>
                                <?php foreach($errors as $error): ?>
                                    <li><?=$error;?></li>
                                <?php endforeach; ?>
                            </ul>
                    </div>
                <?php endif; ?>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Tipo de Usuário</label>
                            <select class="form-control" name="user-type" id="user-type">
                                <option value="">---Selecione---</option>
                                <option value="1">1. Administrador</option>
                                <option value="2">2. Analista</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>Nome</label>
                            <input type="text" class="form-control" value="" id="name" name="name" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Primeiro nome</label>
                            <input type="text" class="form-control" value="" id="first-name" name="first-name" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CPF</label>
                            <input type="text" class="form-control" value="" id="user-id" name="user-id" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>CNPJ</label>
                            <input type="text" class="form-control" value="" id="company-id" name="company-id" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Data de Nascimento</label>
                            <input type="text" class="form-control" value="" id="birthdate" name="birthdate" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="form-group ">
                            <label>E-mail</label>
                            <input type="email" class="form-control" value="" id="email" name="email" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Senha</label>
                            <input type="password" class="form-control" value="" id="password" name="password" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                        <div class="form-group ">
                            <label>Confirme sua senha</label>
                            <input type="password" class="form-control" value="" id="passwordConfirmation" name="passwordConfirmation" autocomplete="off">
                        </div>
                    </div>
                </div>
                <button class="btn btn-success text-center" style="float:right" type="submit"><i class="fa fa-check-circle"></i> Cadastrar</button>
            </div>
        </div>
    </form>
</div>
