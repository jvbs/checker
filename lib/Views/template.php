<?php $user = \Auth::isLogged(); ?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"> -->
    <!-- Plugins -->
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/plugins/css/perfect-scrollbar.css">
    <!-- Theme -->
    <link rel="stylesheet" href="<?=getBaseURL()?>/assets/css/checker.min.css">

    <title>DF Checker</title>
</head>
<body>
<div class="full-page">
    <div class="sidebar-container">
        <aside  class="sidebar sidebar-left sidebar-fixed sidebar-dark">
            <div class="scroll">
                <div class="sidebar-header text-white bg-info">
                    <div class="sidebar-brand d-flex justify-content-between align-items-center">
                        <div class="title text-truncate">DF Checker</div>
                        <div class="logo align-items-center justify-content-around">
                            <!-- <i class="material-icons">all_inclusive</i> -->
                            <i>DF</i>
                        </div>
                    </div>
                </div>
                <div class="sidebar-nav-container">
                    <ul class="list-nav sidebar-nav list-nav-dark list-nav-dark-info">
                        <li class="list-nav-group-title">
                            <span>MENU</span>
                        </li>
                        <li class="list-nav-item">
                            <a href="<?=getBaseURL()?>" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">home</i>
                                </span>
                                <span class="list-nav-label">Dashboard</span>
                            </a>
                        </li>
                        <?php if($user->getUserLevel() == 1){ ?>
                        <li class="list-nav-item">
                            <a href="#" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">people</i>
                                </span>
                                <span class="list-nav-label">Clientes</span>
                                <span class="list-nav-expand">
                                    <i class="material-icons">add</i>
                                </span>
                            </a>
                            <ul class="list-nav-child">
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/cadastrar-clientes" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Cadastrar Clientes</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li class="list-nav-item">
                            <a href="#" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">assessment</i>
                                </span>
                                <span class="list-nav-label">PCP</span>
                                <span class="list-nav-expand">
                                    <i class="material-icons">add</i>
                                </span>
                            </a>
                            <ul class="list-nav-child">
                                <!-- <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/novo-orcamento" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Novo Orçamento</span>
                                    </a>
                                </li>-->
                                <?php if($user->getUserLevel() == 1){ ?>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/nova-ordem-de-servico" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Nova OS</span>
                                    </a>
                                </li>
                                <?php } else { ?>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/executar-ordem-de-servico" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Execução OS</span>
                                    </a>
                                </li>
                                <?php } ?>
                                <!-- <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/status-orcamento" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Status dos Orçamentos</span>
                                    </a>
                                </li>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/status-os" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Status das OS</span>
                                    </a>
                                </li>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/controle-qualidade" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Controle de Qualidade</span>
                                    </a>
                                </li>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/indicadores-produtividade" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Indicadores de Produtividade</span>
                                    </a>
                                </li>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Cronograma Projetado</span>
                                    </a>
                                </li>
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Cronograma Realizado</span>
                                    </a>
                                </li> -->
                            </ul>
                        </li>
<!--                         <li class="list-nav-item">
                            <a href="#" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">show_chart</i>
                                </span>
                                <span class="list-nav-label">Relatórios</span>
                            </a>
                        </li> -->
                        <li class="list-nav-group-title">
                            <span style="text-transform:uppercase">administrativo</span>
                        </li>
                        <?php if($user->getUserLevel() == 1){ ?>
                        <li class="list-nav-item">
                            <a href="<?=getBaseURL()?>/novo-usuario" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">person_add</i>
                                </span>
                                <span class="list-nav-label">Novo Usuário</span>
                            </a>
                        </li>
                        <?php } ?>
                        <li class="list-nav-item">
                            <a href="#" class="list-nav-link">
                                <span class="list-nav-icon">
                                    <i class="material-icons">settings</i>
                                </span>
                                <span class="list-nav-label">Configurações</span>
                                <span class="list-nav-expand">
                                    <i class="material-icons">add</i>
                                </span>
                            </a>
                            <ul class="list-nav-child">
                                <li class="list-nav-item list-nav-child-item">
                                    <a href="<?=getBaseURL()?>/configuracoes/meus-dados" class="list-nav-link">
                                        <span class="list-nav-icon list-nav-icon-sm">
                                            <i class="material-icons">keyboard_arrow_right</i>
                                        </span>
                                        <span class="list-nav-label">Meus Dados</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </aside>
    </div>
    <div class="page">
        <div class="topbar-container">
            <nav class="navbar bg-white text-secondary navbar-expand-sm py-0 topbar fixed">
                <button class="btn btn-flat-secondary btn-sm btn-icon mr-1 no-shadow d-none d-xl-block sidebar-toggle">
                    <i class="material-icons">menu</i>
                </button>
                <button class="btn btn-flat-secondary btn-sm btn-icon mr-1 d-xl-none no-shadow sidebar-hide">
                    <i class="material-icons">menu</i>
                </button>
                <div class="navbar-text ml-3 d-none d-sm-block">
                    <h5 class="m-0 text-dark"><!-- Nav Text --></h5>
                </div>

                <div class="ml-auto d-sm-flex">
                    <div class="d-none d-sm-flex align-items-center">
                        <input class="form-control collapsed topbar-search border-top-0 border-left-0 border-right-0" type="search" placeholder="Search" aria-label="Search">
                    </div>
                    <ul class="navbar-nav">
<!--                         <li class="nav-item dropdown no-caret mr-3">
                            <a class="dropdown-toggle nav-link text-center" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">notifications_none</i>
                                <span class="badge badge-warning">2</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right py-0" style="width:20rem" aria-labelledby="navbarDropdown">
                                <ul class="list-group list-group-linked border-top-0 border-left-0 border-right-0 list-group-linked">
                                    <li class="list-group-item">
                                        <div class="px-3 py-3">
                                            <h6 class="m-0">Notifications (<span class="text-warning">12</span>)</h6>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <a class="media py-2 px-3">
                                            <div class="media-box media-box-xs rounded-circle bg-info text-white mt-1"><i class="material-icons">event</i></div>
                                            <div class="media-body ml-3">
                                                <p class="text-sm">
                                                    Alex has invited you to an event on<br>
                                                    <span class="text-secondary ">12 jan 2012</span>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a class="media py-2 px-3">
                                            <div class="media-box media-box-xs rounded-circle bg-success text-white mt-1"><i class="material-icons">mail</i></div>
                                            <div class="media-body ml-3">
                                                <p class="text-sm">
                                                    New Company Policy for Holiday Season Parties<br>
                                                    <span class="text-secondary ">12 jan 2012</span>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="list-group-item">
                                        <a class="media py-2 px-3">
                                        <div class="media-box media-box-xs rounded-circle bg-secondary text-white mt-1"><i class="material-icons">event</i></div>
                                        <div class="media-body ml-3">
                                            <p class="text-sm">
                                                Your Leave application has been approved<br>
                                                <span class="text-secondary ">12 jan 2012</span>
                                            </p>
                                        </div>
                                        </a>
                                    </li>
                                    <li class="list-group-item text-center">
                                        <a class="dropdown-item empty-link text-info px-3 py-3">Load More</a>
                                    </li>
                                </ul>
                            </div>
                        </li> -->
                        <li class="nav-item dropdown no-caret ml-4" >
                            <a class="text-xs nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="http://via.placeholder.com/256x256" class="img-thumb-xs mr-1 align-middle"  alt="">
                                <span><?=$user->getNickname();?></span>
                                <i class="material-icons">expand_more</i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?=getBaseURL()?>/configuracoes/meus-dados"><i class="material-icons">person</i> Meus Dados</a>
                                <a class="dropdown-item" href="<?=getBaseURL()?>/logout"><i class="material-icons">power_settings_new</i> Sair</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="page-content">
            <?php
                if(isset($viewName)){
                    $path = viewsPath().$viewName.'.php';
                    if(file_exists($path)){
                        require_once $path;
                    }
                }
            ?>
        </div>
    </div>
</div>

<script src="<?=getBaseURL()?>/assets/plugins/js/jquery.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/popper.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/bootstrap.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/perfect-scrollbar.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/hammer.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/sidebar.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/collapsible-nav.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/color/color.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/colors.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/settings.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/jquery.mask.min.js"></script>
<script src="<?=getBaseURL()?>/assets/js/card.min.js"></script>
<!-- [START] Essential Scripts -->
<script src="<?=getBaseURL()?>/assets/js/checker.min.js"></script>
<script src="<?=getPageScript()?>"></script>
<!-- [END] Essential Scripts -->
<script src="<?=getBaseURL()?>/assets/plugins/js/Chart.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/d3.min.js"></script>
<script src="<?=getBaseURL()?>/assets/plugins/js/c3.min.js"></script>
</body>
</html>
