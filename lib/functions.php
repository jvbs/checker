<?php
function isDevEnv(){
    return ENV == 'dev';
}

function viewsPath(){
    return APP_ROOT_PATH.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR;
}

function logsPath(){
    return APP_ROOT_PATH .DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR;
}

function getBaseURL(){
    return sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        $_SERVER['SERVER_PORT'] == 80 ? '' : ':' .$_SERVER['SERVER_PORT']
    );
}


function getCurrentURL(){
    return getBaseURL().$_SERVER['REQUEST_URI'];
}

function getPageScript(){
    return getBaseURL().'/assets/js/'.str_replace(DIRECTORY_SEPARATOR, '', $_SERVER['REQUEST_URI'].'.min.js');
}

function redirect($url){
    header('Location: '.$url);
    exit;
}
